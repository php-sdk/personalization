<?php

use Econda\RecEngine\Client\Request\RequestModel;
use Econda\RecEngine\Context\Context;

class ClientRequestTest extends PHPUnit_Framework_TestCase
{
	use \Econda\Test\StandardGetterSetterTrait;
	
	public function testGetSetWidgetId()
	{
		$req = new RequestModel();
		$this->_testNumericGetSet($req, 'widgetId', null);
	}
		
	public function testGetSetStartIndex()
	{
		$req = new RequestModel();
		$this->_testNumericGetSet($req, 'startIndex', 0);
	}

	public function testGetSetStartChunkSize()
	{
		$req = new RequestModel();
		$this->_testNumericGetSet($req, 'chunkSize', null);
	}
	
	public function testGetSetIncludeWidgetDetails()
	{
		$req = new RequestModel();
		$this->_testBooleanGetSet($req, 'includeWidgetDetails', true);
	}

    public function testGetSetExcludeProductIds()
    {
        $req = new RequestModel();
        $this->assertEquals([], $req->getExcludeProductIds());
        $this->assertSame($req, $req->setExcludeProductIds('ABC'));
        $this->assertEquals(['ABC'], $req->getExcludeProductIds());
        $this->assertEquals(['A1','A2'], $req->setExcludeProductIds(['A1','A2'])->getExcludeProductIds());
    }

	public function testRequestModelWithContextInConstructor()
	{
		$productId = 'TEST_PRODUCT_ID_1';

		$req = new RequestModel([
			'context' => new Context([
				'productIds' => [$productId]
			])
		]);

		$this->assertEquals($productId, $req->getContext()->getProductIds()[0]);
	}

    public function testRequestModelWithContextAsAssocArrayInConstructor()
	{
		$productId = 'TEST_PRODUCT_ID_1';

		$req = new RequestModel([
			'context' => [
				'productIds' => [$productId]
			]
		]);

		$this->assertEquals($productId, $req->getContext()->getProductIds()[0]);
	}


    public function testGetSetContext()
    {
        $req = new RequestModel();
        $req->setContext((new Context(['productIds' => 'ABC'])));
        $this->assertEquals('ABC', $req->getContext()->getProductIds()[0]);
    }
}