php-sdk
===============

Helper classes to access econda recommendations service. This service provides
product recommendations for online shops.

### Installing via Composer

The recommended way to install this library is through [Composer](http://getcomposer.org).


```bash
# Install Composer
curl -sS https://getcomposer.org/installer | php

# Add lib as dependency
php composer.phar require econda/personalization
```

After installing, you need to require Composer's autoloader:

```php
require 'vendor/autoload.php';
```

### Documentation
For further documentation, please visit our support portal https://support.econda.de

### Contribute
You can clone the repository after creating an account at https://git.econda.de

### Running Tests

Preconditions:
* run composer install first to load dependencies and generate autoload configuration

```bash
cd /PROJECT_ROOT/tests
./run.sh
``` 
